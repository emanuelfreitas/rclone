FROM alpine

LABEL maintainer="Emanuel Freitas <emanuelfreitas@outlook.com>"

RUN ARCH=$(uname -m) && \
    if [ "$ARCH" == "armv7l" ]; then export ARCH="arm"; fi && \
    if [ "$ARCH" == "x86_64" ]; then export ARCH="amd64"; fi && \
    if [ "$ARCH" == "aarch64" ]; then export ARCH="arm64"; fi && \
    apk add --no-cache wget unzip bash && \
    cd /tmp && \
    wget -q https://downloads.rclone.org/rclone-current-linux-$ARCH.zip && \
    unzip /tmp/rclone-current-linux-$ARCH.zip && \
    mv /tmp/rclone-*/rclone /usr/bin && \
    install -d -m 777 /config && \
    rm -rf /tmp/* /var/tmp/* /var/cache/apk/*

CMD ["rclone", "listremotes"]
ENTRYPOINT ["rclone"]